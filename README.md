## Qomma - CSV SQL CLI Tool

##### Important Notes:
- I don't use Python very often, so you might see what could look like a different coding style, and perhaps some blocks of code that could have been shortened using special Python's function and techniques.
- I've written unit and integration test, but it's not 100% or near full coverage, due to time restrictions.
- Better Exceptions and logging could have been done, like creating or using more specific Exceptions. So that's something to consider if I have more time.
- Querying data works for most cases, but I won't say that the tool is bug-free at the moment. And validation isn't that strong.

#### Overview

- This is an interactive CLI tool.
- A simple Python 3.8 based CLI tool that uses no external libraries.
- Unit and integration tested using Python's unittest.
- Some important documentation/comments is provided at the header of almost all files.
- Some test data (csv files) is provided under ./test/data
- The tool starts by executing the special bootstrap file and provide a directory as parameter.
- The bootstrap will build the main context and inject dependencies.

#### The Idea and the Implementation
For a POC of a CLI tool that enables user to execute SQL statements against CSV files as tables, I approached this case as follows:

1. How the user should interact with the tool and what minimum functionality I could provide.
2. How validation rules and feedback messages should be applied.
3. At this stage, data can be loaded into memory at once.
4. The First step of processing SQL query is to make it easier to navigate through and work on by cleaning it, splitting it, and tokenize it.
5. Second step is to use the tokenized statement to build a Plan, a plan will control the flow of execution.
6. Each main Plan - in our case Select Plan - has dependencies on other sub-plans (like From, Tables, ..), which all can be reused. And it constructs a chain of actions to apply to the result.
7. So the main general flow is Tokenize -> Plan -> Execute.
8. Select statement plan is executed in reverse order: From (get data) -> Where (filter if needed) -> Select (how data should be returned, and what columns to include).
9. Condition Plan for Where Statement is one of the most complicated plans, please read the comments to understand who it works.
10. In the end, the result will be printed as a table.

#### How to Use

Run the test:


```
$ python3 -m unittest discover test/
```


Run the tool:


    $ python3 qomma.py /path/to/directory/where/csv/files

Quit the tool:


    $ q

Example:


    xx@xx:~/qomma$ python3 qomma.py /home/xx/Downloads
    2 Tables:
    techcrunch
    techcrunch-test-data
    /home/hisham/Downloads=#select company,numEmps from techcrunch where numEmps=14;
    5 records returned: 
    company        numEmps        
    Scribd         14             
    Scribd         14             
    Scribd         14             
    SayNow         14             
    DailyStrength  14             
    /home/hisham/Downloads=# q
    xx@xx:~/qomma$

